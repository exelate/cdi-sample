/*
 * eXelate samples suite
 * 
 * @author {@link "mailto:mottyc@exelate.com"}
 * 
 * Copyright (C) 2012 eXelate, ALL RIGHTS RESERVED
 */

package com.exelate.sample.zookeeper;

import com.exelate.sample.cditest.Interfaces.IMonitor;

/**
 * ZooKeeper monitoring client implementation
 * @author mottyc
 *
 */
public class MonitorZooKeeper implements IMonitor {

	/**
	 * Send heart beat signal
	 */
	public void sendHeartBeat() {
		System.out.println("Sending heart beat signal to ZooKeeper");
	}
	
	/**
	 * Report measured value for a single measurement
	 * @param type - the kpi identifier
	 * @param value - the measured value
	 */
	public void reportKpi(int kpi, double value) {
		String msg = String.format("Sending KPI %d value: %f to ZooKeeper", kpi, value);
		System.out.println(msg);
	}
	
	/**
	 * Report measured value average and standard deviation
	 * @param kpi
	 * @param avg
	 * @param stdev
	 */
	public void reportKpi(int kpi, double avg, double stdev) {
		String msg = String.format("Sending KPI %d value: %f and stdev: %f to ZooKeeper", kpi, avg, stdev);
		System.out.println(msg);
	}

}
