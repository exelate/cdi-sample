/*
 * eXelate samples suite
 * 
 * @author {@link "mailto:mottyc@exelate.com"}
 * 
 * Copyright (C) 2012 eXelate, ALL RIGHTS RESERVED
 */

/*
 * eXelate samples suite
 * 
 * @author {@link "mailto:mottyc@exelate.com"}
 * 
 * Copyright (C) 2012 eXelate, ALL RIGHTS RESERVED
 */

package com.exelate.sample.redis;

import java.util.concurrent.Future;

import javax.enterprise.inject.Alternative;

import com.exelate.sample.cditest.Interfaces.IMessagingProvider;

/**
 * Redis monitoring client implementation
 * This is an alternative concrete implementation
 * 
 * @author mottyc
 */
@Alternative
public class RedisClient implements IMessagingProvider {
	
	/**
	 * Send addressed message to specific addressee (queue)
	 * @param queue The queue name
	 * @param message The message
	 */
	public void send(String queue, String message) {
		String msg = String.format("Sending message: %s to ZMQ queue: %s", message, queue);
		System.out.println(msg);
	}
	
	/**
	 * publish message to all subscribers
	 * @param topic The topic name
	 * @param message The message
	 */
	public void publish(String topic, String message) {
		String msg = String.format("Publishing message: %s to ZMQ topic: %s", message, topic);
		System.out.println(msg);
	}
	
	/**
	 * Subscribe to a topic and invoke a handler when a message arrive
	 * @param topic The topic name
	 * @param handler The future method to invoke
	 */
	public void subscribe(String topic, Future<String> handler) {
		String msg = String.format("Subscribe to topic: %s", topic);
		System.out.println(msg);
	}

}
