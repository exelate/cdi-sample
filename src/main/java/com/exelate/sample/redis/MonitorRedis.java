/*
 * eXelate samples suite
 * 
 * @author {@link "mailto:mottyc@exelate.com"}
 * 
 * Copyright (C) 2012 eXelate, ALL RIGHTS RESERVED
 */

package com.exelate.sample.redis;

import javax.enterprise.inject.Alternative;

import com.exelate.sample.cditest.Interfaces.IMonitor;

/**
 * Redis monitoring client implementation
 * This is an alternative concrete implementation
 * 
 * @author mottyc
 *
 */
@Alternative
public class MonitorRedis implements IMonitor {

	/**
	 * Send heart beat signal
	 */
	public void sendHeartBeat() {
		System.out.println("Sending heart beat signal to Redis");
	}
	
	/**
	 * Report measured value for a single measurement
	 * @param type - the kpi identifier
	 * @param value - the measured value
	 */
	public void reportKpi(int kpi, double value) {
		String msg = String.format("Sending KPI %d value: %f to Redis", kpi, value);
		System.out.println(msg);
	}
	
	/**
	 * Report measured value average and standard deviation
	 * @param kpi
	 * @param avg
	 * @param stdev
	 */
	public void reportKpi(int kpi, double avg, double stdev) {
		String msg = String.format("Sending KPI %d value: %f and stdev: %f to Redis", kpi, avg, stdev);
		System.out.println(msg);
	}
}
