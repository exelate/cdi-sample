/*
 * eXelate samples suite
 * 
 * @author {@link "mailto:mottyc@exelate.com"}
 * 
 * Copyright (C) 2012 eXelate, ALL RIGHTS RESERVED
 */

package com.exelate.sample.cditest;

import java.util.List;

import javax.enterprise.event.Observes;
import javax.inject.Inject;

import org.jboss.weld.environment.se.bindings.Parameters;
import org.jboss.weld.environment.se.events.ContainerInitialized;

import com.exelate.sample.cditest.Interfaces.IMessagingProvider;
import com.exelate.sample.cditest.Interfaces.IMonitor;

/**
 * This sample application does not require static main entry point.
 * This class will be initialized and started by the CDI container. 
 * In this case we can start with two entry points in the same class
 * or even in two different classes
 * 
 * @author mottyc
 *
 */
public class TwoEntryPointsApplication {

	@Inject
	private IMessagingProvider messageBus;
	
	@Inject
	private IMonitor monitor;
	
	/**
	 * This is the entry point.
	 * In a standard configuration, there is no "static void main" entry point.
	 * One of the classes contains "main" method which will be called by the CDI
	 * manager (Weld) after all classes were initialized and injected.
	 * 
	 * When deploying it as a stand alone jar, remember that since ther is no main,
	 * you should use: "org.jboss.weld.environment.se.StartMain" as the main class
	 * in your jar. Command line arguments shall be injected to the parameters list.
	 * 
	 * @param event Initialized event parameters
	 * @param parameters Command line arguments
	 */
	public void main(@Observes ContainerInitialized event, @Parameters List parameters) {
		System.out.println("This is the starting poin");
		
		// Now start your business logic
		this.doSomething();
    }
	
	/**
	 * The business logic, here we shall use the injected classes
	 * @throws InterruptedException
	 */
	public void doSomething() {
		
		System.out.println("I'm doing something...");
		for (int counter = 0; counter < 10; counter++) {

			// Send a message
			monitor.sendHeartBeat();
			
			// report KPI
			monitor.reportKpi(1, 12.345 * counter);
			
			// send a message
			messageBus.send("WorkerQueue", String.format("Message %d To Process", counter));
			
			try {Thread.sleep(1000);} catch (InterruptedException e) {}
		}
	}
	
	/**
	 * Weld has the ability to have multiple main methods in multiple classes 
	 * by having other methods use the @Observes annotation with the ContainerInitiatized event. 
	 * Weld will execute one of these at a time. One will finish and then the next will start. 
	 * 
	 * @param event Initialized event parameters
	 * @param parameters Command line arguments
	 */
	public void secondMain(@Observes ContainerInitialized event, @Parameters List parameters) {
		System.out.println("This is the second starting poin");
		
		// Now start your business logic
		this.doSomethingElse();
    }
	
	/**
	 * The business logic, here we shall use the injected classes
	 * @throws InterruptedException
	 */
	public void doSomethingElse() {
		
		System.out.println("I'm doing something else...");
		try {Thread.sleep(1000);} catch (InterruptedException e) {}
	}
}
