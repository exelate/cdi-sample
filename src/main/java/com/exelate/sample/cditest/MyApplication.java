/*
 * eXelate samples suite
 * 
 * @author {@link "mailto:mottyc@exelate.com"}
 * 
 * Copyright (C) 2012 eXelate, ALL RIGHTS RESERVED
 */

package com.exelate.sample.cditest;

import javax.inject.Inject;
import com.exelate.sample.cditest.Interfaces.IMessagingProvider;
import com.exelate.sample.cditest.Interfaces.IMonitor;

/**
 * This is the application class which will be initialized by the  main procedure
 * in the statis entry point.
 * 
 * @author mottyc
 *
 */
public class MyApplication {

	// Interface to message bus implementation, will be injected by the CDI container
	@Inject
	private IMessagingProvider messageBus;
	
	// Interface to m implementation, will be injected by the CDI container
	@Inject
	private IMonitor monitor;
	
	/**
	 * The business logic, here we shall use the injected classes
	 * @throws InterruptedException
	 */
	public void doSomething() {
		
		System.out.println("I'm doing something...");
		for (int counter = 0; counter < 10; counter++) {

			// Send a message
			monitor.sendHeartBeat();
			
			// report KPI
			monitor.reportKpi(1, 12.345 * counter);
			
			// send a message
			messageBus.send("WorkerQueue", String.format("Message %d To Process", counter));
			
			try {Thread.sleep(1000);} catch (InterruptedException e) {}
		}
	}
}
