/*
 * eXelate samples suite
 * 
 * @author {@link "mailto:mottyc@exelate.com"}
 * 
 * Copyright (C) 2012 eXelate, ALL RIGHTS RESERVED
 */

package com.exelate.sample.cditest.Interfaces;

/**
 * Monitor client interface providing heart beat and KPI reporting
 * @author mottyc
 *
 */
public interface IMonitor {
	
	/**
	 * Send heart beat signal
	 */
	void sendHeartBeat();
	
	/**
	 * Report measured value for a single measurement
	 * @param type - the kpi identifier
	 * @param value - the measured value
	 */
	void reportKpi(int kpi, double value);
	
	/**
	 * Report measured value average and standard deviation
	 * @param kpi
	 * @param avg
	 * @param stdev
	 */
	void reportKpi(int kpi, double avg, double stdev);
}
