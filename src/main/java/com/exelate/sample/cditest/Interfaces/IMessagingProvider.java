/*
 * eXelate samples suite
 * 
 * @author {@link "mailto:mottyc@exelate.com"}
 * 
 * Copyright (C) 2012 eXelate, ALL RIGHTS RESERVED
 */

package com.exelate.sample.cditest.Interfaces;

import java.util.concurrent.Future;

/**
 * Messaging provider interface
 * @author mottyc
 *
 */
public interface IMessagingProvider {
	
	/**
	 * Send addressed message to specific addressee (queue)
	 * @param queue The queue name
	 * @param message The message
	 */
	void send(String queue, String message);
	
	/**
	 * publish message to all subscribers
	 * @param topic The topic name
	 * @param message The message
	 */
	void publish(String topic, String message);
	
	/**
	 * Subscribe to a topic and invoke a handler when a message arrive
	 * @param topic The topic name
	 * @param handler The future method to invoke
	 */
	void subscribe(String topic, Future<String> handler);
}
