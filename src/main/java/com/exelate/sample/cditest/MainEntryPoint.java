/*
 * eXelate samples suite
 * 
 * @author {@link "mailto:mottyc@exelate.com"}
 * 
 * Copyright (C) 2012 eXelate, ALL RIGHTS RESERVED
 */

package com.exelate.sample.cditest;

import org.jboss.weld.environment.se.Weld;
import org.jboss.weld.environment.se.WeldContainer;

/**
 * Sample initialization of bean container using java style 
 * entry point "public static void main(...)"
 * 
 * @author mottyc
 *
 */
public class MainEntryPoint {
	
	/**
	 * Main entry point
	 * @param args
	 */
	public static void main(String[] args) {

		// Create new Weld Container
		Weld weld = new Weld();
		
		// All the components initialization and wiring is done here
		WeldContainer container = weld.initialize();
		
		// Get application instance
		// This instance will be initialized with all required components 
		MyApplication app = container.instance().select(MyApplication.class).get();
		
		// Do your things
		app.doSomething();
		
		// Close container
		weld.shutdown();
	}
}
