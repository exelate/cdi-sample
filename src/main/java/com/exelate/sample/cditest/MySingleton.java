/*
 * eXelate samples suite
 * 
 * @author {@link "mailto:mottyc@exelate.com"}
 * 
 * Copyright (C) 2012 eXelate, ALL RIGHTS RESERVED
 */

package com.exelate.sample.cditest;

import javax.inject.Singleton;

@Singleton
public class MySingleton {

	public void doSomething() {
		System.out.println("I'm doing something");
	}
}
