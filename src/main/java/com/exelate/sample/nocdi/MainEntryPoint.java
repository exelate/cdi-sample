/*
 * eXelate samples suite
 * 
 * @author {@link "mailto:mottyc@exelate.com"}
 * 
 * Copyright (C) 2012 eXelate, ALL RIGHTS RESERVED
 */

package com.exelate.sample.nocdi;

import com.exelate.sample.cditest.Interfaces.IMessagingProvider;
import com.exelate.sample.cditest.Interfaces.IMonitor;
import com.exelate.sample.zmq.ZeroMqClient;
import com.exelate.sample.zookeeper.MonitorZooKeeper;

/**
 * Sample bootstrapping and initialization from the main entry point 
 * 
 * @author mottyc
 *
 */
public class MainEntryPoint {
	
	/**
	 * Main entry point
	 * @param args
	 */
	public static void main(String[] args) {

		// Initialize message bus
		IMessagingProvider messageBus = new ZeroMqClient();
		
		// Initialize monitor
		IMonitor monitor = new MonitorZooKeeper();
		
		// Initialize Application and inject dependencies
		MyApplication app = new MyApplication();
		app.setMessageBus(messageBus);
		app.setMonitor(monitor);
		
		// Or use constructor injection
		//MyApplication app2 = new MyApplication(messageBus, monitor);
		
		// Do your things
		app.doSomething();
		
	}
}
