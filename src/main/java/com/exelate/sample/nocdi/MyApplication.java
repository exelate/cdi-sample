/*
 * eXelate samples suite
 * 
 * @author {@link "mailto:mottyc@exelate.com"}
 * 
 * Copyright (C) 2012 eXelate, ALL RIGHTS RESERVED
 */

package com.exelate.sample.nocdi;

import com.exelate.sample.cditest.Interfaces.IMessagingProvider;
import com.exelate.sample.cditest.Interfaces.IMonitor;

public class MyApplication {

	// Interface to message bus implementation
	private IMessagingProvider messageBus;
	
	// Interface to m implementation
	private IMonitor monitor;
	
	public IMessagingProvider getMessageBus() {
		return messageBus;
	}

	public void setMessageBus(IMessagingProvider messageBus) {
		this.messageBus = messageBus;
	}

	public IMonitor getMonitor() {
		return monitor;
	}

	public void setMonitor(IMonitor monitor) {
		this.monitor = monitor;
	}

	/**
	 * Default constructor
	 */
	public MyApplication() { }
	
	/**
	 * Constructor with injected parameters
	 * @param messageBus The message bus implementation
	 * @param monitor The monitor implementation
	 */
	public MyApplication(IMessagingProvider messageBus, IMonitor monitor) {
		this.messageBus = messageBus;
		this.monitor = monitor;
	}
	
	/**
	 * The business logic, here we shall use the injected classes
	 * @throws InterruptedException
	 */
	public void doSomething() {
		
		System.out.println("I'm doing something...");
		for (int counter = 0; counter < 10; counter++) {

			// Send a message
			monitor.sendHeartBeat();
			
			// report KPI
			monitor.reportKpi(1, 12.345 * counter);
			
			// send a message
			messageBus.send("WorkerQueue", String.format("Message %d To Process", counter));
			
			try {Thread.sleep(1000);} catch (InterruptedException e) {}
		}
	}
}
